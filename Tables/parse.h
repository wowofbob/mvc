#ifndef META_MVC_PARSE_H_
#define META_MVC_PARSE_H_

#include <string>
#include <utility>

namespace meta {

	typedef
		std::string::const_iterator str_citer;

	bool is_space(char c);

	bool is_not_space(char c);

	void pass_space(
		str_citer& itr, const str_citer& end);

	void pass_until(char c,
		str_citer& itr, const str_citer& end);

	bool is_digit(char c);

	bool is_zero(
		str_citer itr, const str_citer& end);
	bool is_zero(const std::string& str);

	bool is_number(
		str_citer itr, const str_citer& end);
	bool is_number(const std::string& str);

	void pass_token(
		str_citer& itr, const str_citer& end);

	std::string take_until(char c,
		str_citer& itr, const str_citer& end);

	std::string take_token(
		str_citer& itr, const str_citer& end);

	std::string take_name(
		str_citer& itr, const str_citer& end);

	class parser {
		std::string _Str;
		str_citer _Itr;
		str_citer _End;
	public:
		
		std::string token();
		std::pair<
			std::string, bool> complex();
		
		inline bool has_next() const;
		
		parser(const std::string& str);
		parser(std::string&& str);
	};

	bool parser::has_next() const { return _Itr != _End; }

}

#endif//META_MVC_PARSE_H_