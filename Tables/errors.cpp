#include "stdafx.h"
#include "errors.h"

#include <iostream>

namespace meta {

	void print_use_help() {
		std::cout <<
			"    Type 'help' for more information"
			<< std::endl;
	}

	void print_tying_err() {
		std::cout <<
			"    Entered view type is already tied"
			<< std::endl;
	}

	void print_untying_err() {
		std::cout <<
			"    Entered view type was not tied"
			<< std::endl;
	}

	void print_view_type_err() {
		std::cout <<
			"    Entered view type is not supported"
			<< std::endl;
		print_use_help();
	}

	void print_unknown_cmd_err() {
		std::cout <<
			"    Unknown command. Type 'help' to see availables one"
			<< std::endl;
	}

	void print_param_missing_err() {
		std::cout <<
			"    Parameter was missed"
			<< std::endl;
	}

	void print_parse_number_err() {
		std::cout <<
			"    Cannot parse number"
			<< std::endl;
	}

	void print_parse_name_err() {
		std::cout <<
			"    Cannot parse name"
			<< std::endl;
	}

	void print_too_long_name_err() {
		std::cout
			<< "    Entered name is too long."
			<< std::endl;
	}

	void print_has_field_err() {
		std::cout <<
			"    Table has already had a field with this name"
			<< std::endl;
	}

	void print_no_field_err() {
		std::cout <<
			"    Table hasn't a field with this name"
			<< std::endl;
	}

	void print_pos_out_of_range_err() {
		std::cout <<
			"    Position is in out of range"
			<< std::endl;
	}

	void print_no_fields_err() {
		std::cout <<
			"     Tables hasn't fields"
			<< std::endl;
	}

	void print_no_values_err() {
		std::cout <<
			"     Tables hasn't values"
			<< std::endl;
	}

}