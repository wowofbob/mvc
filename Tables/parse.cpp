#include "stdafx.h"
#include "parse.h"

#include <algorithm>
#include <string>
#include <utility>

namespace meta {

	bool is_space(char c) {
		return c == ' ';
	}

	bool is_not_space(char c) {
		return !is_space(c);
	}

	void pass_space(
		str_citer& itr, const str_citer& end) {
		itr = std::find_if(itr, end, is_not_space);
	}

	void pass_until(char c,
		str_citer& itr, const str_citer& end) {
		itr = std::find(itr, end, c);
	}

	bool is_digit(char c) {
		return c >= '0' && c <= '9';
	}

	bool is_zero(
		str_citer itr, const str_citer& end) {
			if (itr != end && *itr == '0') {
				return ++itr == end;
			}
			return false;
		}

	bool is_zero(const std::string& str) {
		return is_zero(str.cbegin(), str.cend());
	}

	bool is_number(
		str_citer itr, const str_citer& end) {
		if (itr != end && is_digit(*itr)) {
			return is_zero(itr++, end) || std::all_of(itr, end, is_digit);
		}
		return false;
	}

	bool is_number(const std::string& str) {
		return is_number(str.cbegin(), str.cend());
	}

	void pass_token(
		str_citer& itr, const str_citer& end) {
		pass_until(' ', itr, end);
	}

	std::string take_until(char c,
		str_citer& itr, const str_citer& end) {
		str_citer fst = itr;
		pass_until(c, itr, end);
		str_citer lst = itr;
		return std::string(fst, lst);
	}

	std::string take_token(
		str_citer& itr, const str_citer& end) {
		return take_until(' ', itr, end);
	}

	std::string take_name(
		str_citer& itr, const str_citer& end) {
			char stop = ' ';
			if (*itr == '"') {
				stop = *(itr++);
			}
			std::string
				name(take_until(stop, itr, end));
			++itr;
			return name;
		}

	parser::
		parser(const std::string& str) :
		_Str(str),
		_Itr(_Str.cbegin()),
		_End(_Str.cend())
	{
		pass_space(_Itr, _End);
	}

	parser::
		parser(std::string&& str) :
		_Str(std::move(str)),
		_Itr(_Str.cbegin()),
		_End(_Str.cend())
	{
		pass_space(_Itr, _End);
	}

	std::string parser::token() {
		std::string tkn(take_token(_Itr, _End));
		pass_space(_Itr, _End);
		return tkn;
	}

	std::pair<
		std::string, bool>
		parser::complex() {
			if (*_Itr == '"') {
				std::string
					tkn(take_until('"', ++_Itr, _End));
				if (_Itr == _End) {
					return
						std::make_pair(tkn, false);
				}
				pass_space(++_Itr, _End);
				return
					std::make_pair(tkn, true);
			}
			return
				std::make_pair(token(), true);
		}

}