#include "stdafx.h"
#include "controller.h"

#include <map>
#include <string>

#include "model.h"
#include "view.h"

namespace meta {

	void controller::tie_view
		(const std::string& view_name, view* inst)
			{
				_Model.tie(inst);
				_Views.insert(std::make_pair(view_name, inst));
			}

	void controller::untie_view(
		const std::string& view_name, view* inst)
		{
			view* to_erase = _Views[view_name];
			_Views.erase(view_name);
			_Model.untie(to_erase);
		}

	bool controller::has_view(
		const std::string& view_name) const {
			return
				_Views.find(view_name) != _Views.cend();
		}


	view* controller::get_view(
		const std::string& view_name) const {
		view* v = _Views.find(view_name)->second;
		return v;
	}

	void controller::
		show() {
			std::map<std::string, view*>
				::const_iterator
				itr(_Views.cbegin()),
				end(_Views.cend());
			while (itr != end) {
				(itr++)->second->update();
			}
		}

	controller::
		controller(model& m) : _Model(m), _Views() {}

	controller::~controller() {}

}