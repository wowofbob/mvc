#include "stdafx.h"
#include "table.h"

#include <assert.h>

#include <list>
#include <map>
#include <string>

#include "field.h"

namespace meta {

	table::table() : _Name2field(), _Fields(), _Val_size(0) {}

	table::const_iterator table::cbegin() const {
		return _Fields.cbegin();
	}
	
	table::const_iterator table::cend() const {
		return _Fields.cend();
	}

	field const& table::get(std::string const& name) const {
		auto target(_Name2field.find(name));
		assert(target != _Name2field.cend());
		return *(target->second);
	}

	field& table::get(std::string const& name) {
		auto target(_Name2field.find(name));
		assert(target != _Name2field.cend());
		return *(target->second);
	}

	size_t table::fields_count() const { return _Fields.size(); }
	size_t table::values_count() const { return _Val_size; }

	bool table::
		has_name(std::string const& name) const {
			return _Name2field.find(name) != _Name2field.cend();
		}


	void table::stretch(unsigned int init_val) {
		assert(fields_count() > 0);
		auto first(_Fields.begin()), last(_Fields.end());
		while (first != last) {
			(first++)->second.stretch(init_val);
		}
		++_Val_size;
		notify();
	}

	void table::compress() {
		assert(fields_count() > 0 && values_count() > 0);
		auto first(_Fields.begin()), last(_Fields.end());
		while (first != last) {
			(first++)->second.compress();
		}
		--_Val_size;
		notify();
	}

	void table::expand(std::string const& name, unsigned int init_val) {
		_Fields.push_back(std::make_pair(name, field(*this, _Val_size, init_val)));
		_Name2field.insert(std::make_pair(name, &_Fields.back().second));
		notify();
	}

	void table::expand(std::string&& name, unsigned int init_val) {
		_Fields.push_back(std::make_pair(std::move(name), field(*this, _Val_size, init_val)));
		_Name2field.insert(std::make_pair(_Fields.back().first, &_Fields.back().second));
		notify();
	}
	
	void table::constrict() {
		_Name2field.erase(_Fields.back().first);
		_Fields.pop_back();
		notify();
	}

}