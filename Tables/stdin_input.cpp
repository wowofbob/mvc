#include "stdafx.h"
#include "stdin_input.h"

#include "parse.h"
#include <iostream>
#include <string>

namespace meta {

	std::string get_input() {
		std::string input;
		std::getline(std::cin, input);
		return input;
	}

	stdin_input::stdin_input() :
		_Str(get_input()),
		_Fst(_Str.cbegin()),
		_Lst(_Str.cend())
	{
		pass_space(_Fst, _Lst);
	}

}