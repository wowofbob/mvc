#ifndef META_MVC_FIELD_H_
#define META_MVC_FIELD_H_

#include <memory>
#include <map>

#include "model.h"

namespace meta {

	class table;

	class field {

		friend class table;

		model const& _Model;

		std::map<size_t, unsigned int> _Vals;

		static std::map<size_t, unsigned int>
			generate_n(size_t n, unsigned int init_val);

		field(model const& parrent,
			  size_t length, unsigned int init_val);

		void stretch(unsigned int init_val);
		void compress();

	public:

		typedef std::map<size_t, unsigned int>
			::const_iterator const_iterator;

		const_iterator cbegin() const;
		const_iterator cend() const;

		unsigned int get(size_t pos) const;

		void set(size_t pos, unsigned int val);

		size_t size() const;

	};

}

#endif//META_MVC_FIELD_H_