#ifndef META_MVC_VIEW_H_
#define META_MVC_VIEW_H_

#include "field.h"
#include "table.h"

#include <list>

namespace meta {

	class view {
	protected:
		const table& _Table;
	public:

		view(const table& t);

		virtual void update() = 0;
		virtual ~view();
	};

	const size_t CELL_WIDE = 10;

	class horizontal : public view {

		void print_name(const std::string& name) const;

		void print_value(unsigned int val) const;

		void print_field(table::const_iterator pos) const;

	public:
	
		horizontal(const table& t);

		void update();
	};

	class vertical : public view {

		void print_val_row(
			std::list<field::const_iterator>& itrs) const;

		void print_vals() const;

	public:

		vertical(const table& t);

		void update();
	};

	class horizontal_diagram : public view {

		unsigned int factor;

	public:

		horizontal_diagram(const table& t, unsigned int f = 10);

		void update();
	};

	class vertical_diagram : public view {

		unsigned int factor;

		void print_val_columns() const;

	public:

		vertical_diagram(const table& t, unsigned int f = 10);

		void update();

	};
}

#endif//META_MVC_VIEW_H_