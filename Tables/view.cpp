#include "stdafx.h"
#include "view.h"

#include <algorithm>
#include <iostream>
#include <vector>

#include "field.h"
#include "table.h"

namespace meta {

	view::~view() {}

	view::view(const table& t) :
		_Table(t) {}

	void print_slash(size_t len) {
		std::vector<char> slash(len + 1, '_');
		slash[len] = 0;
		std::cout << &slash.front() << std::endl;
	}

	void print_in_center(const std::string& str, size_t len) {

		size_t str_len = str.size(),
			free_space = len - str_len,
			space_len = free_space / 2;

		std::vector<char>
			space(space_len + 1, ' ');

		space[space_len] = 0;

		std::cout << &space.front()
			<< str
			<< &space.front();

		if (free_space % 2 == 1)
			std::cout << ' ';

	}

	void print_in_cell(const std::string& str) {
		std::cout << '|';
		print_in_center(str, CELL_WIDE);
		std::cout << '|';
	}

	void horizontal::
		print_name(const std::string& name) const {
			print_in_cell(name);
		}

	void horizontal::
		print_value(unsigned int val) const {
			print_in_center(std::to_string(val), CELL_WIDE);
			std::cout << '|';
		}

	void horizontal::
		print_field(table::const_iterator pos) const {
			print_name(pos->first);
			field::const_iterator
				itr(pos->second.cbegin()),
				end(pos->second.cend());
			while (itr != end) {
				print_value((itr++)->second);
			}
			std::cout << std::endl;
		}


	void horizontal::update() {

		table::const_iterator
			itr(_Table.cbegin()),
			end(_Table.cend());

		size_t
			cells_count =
			_Table.values_count() + 1,
			slash_len =
			cells_count * CELL_WIDE + cells_count + 1;
				

		print_slash(slash_len);
		while (itr != end) {
			print_field(itr++);
			print_slash(slash_len);
		}

	}

	horizontal
		::horizontal(const table& t) :
		view(t) {}

	vertical
		::vertical(const table& t) :
		view(t) {}

	void vertical
		::print_val_row(
		std::list<field::const_iterator>& itrs) const {
			std::list<field::const_iterator>
				::iterator
				itr(itrs.begin()),
				end(itrs.end());
			std::cout << '|';
			while (itr != end) {
				print_in_center(
					std::to_string(((*(itr++))++)->second), CELL_WIDE);
				std::cout << '|';
			}
			std::cout << std::endl;
		}

	void vertical
		::update() {

			size_t cells_cout = _Table.fields_count(),
				slash_len = cells_cout * (CELL_WIDE + 1) + 1;

			std::list<field::const_iterator> itrs;
			table::const_iterator
				itr(_Table.cbegin()),
				end(_Table.cend());
			
			print_slash(slash_len);
			std::cout << '|';
			while (itr != end) {
				print_in_center(itr->first, CELL_WIDE);
				std::cout << '|';
				itrs.push_back((itr++)->second.cbegin());
			}
			std::cout << std::endl;
			print_slash(slash_len);
			for (size_t i = 0; i < _Table.values_count(); ++i) {
				print_val_row(itrs);
				print_slash(slash_len);
			}
		}

	horizontal_diagram
		::horizontal_diagram(const table& t, unsigned int f) :
		view(t), factor(f) {}

	unsigned int field_valsum(const field& f) {
		field::const_iterator
			itr(f.cbegin()),
			end(f.cend());
		unsigned int s = 0;
		while (itr != end) {
			s += (itr++)->second;
		}
		return s;
	}

	void print_horizontal_column(size_t len) {
		std::vector<char> coll(len + 1, 'X');
		coll[len] = 0;
		std::cout << &coll.front();
	}

	void horizontal_diagram
		::update() {
			table::const_iterator
				itr(_Table.cbegin()),
				end(_Table.cend());
			while (itr != end) {
				print_in_center(itr->first, CELL_WIDE);
				std::cout << ": ";
				unsigned int fsum =
					field_valsum((itr++)->second);
				print_horizontal_column(fsum / factor);
				std::cout << std::endl;
			}
		}

	vertical_diagram
		::vertical_diagram(const table& t, unsigned int f) :
		view(t), factor(f) {}

	unsigned int pvalsum(
		const std::pair<std::string, field>& p) {
			return field_valsum(p.second);
		}

	std::vector<unsigned int> 
		valsums(const table& t) {
			std::vector<unsigned int>
				vs(t.fields_count(), 0);
			std::vector<unsigned int>
				::iterator out(vs.begin());
			table::const_iterator
				fst(t.cbegin()),
				lst(t.cend());
			std::transform(fst, lst, out, pvalsum);
			return vs;
		}

	void print_empty_rows(size_t count, size_t cells) {
		for (size_t i = 0; i < count; ++i) {
			std::cout << '|';
			for (size_t j = 0; j < cells; ++j) {
				print_in_center("", CELL_WIDE);
				std::cout << '|';
			}
			std::cout << std::endl;
		}
	}

	void vertical_diagram
		::print_val_columns() const {

			std::cout << std::endl << std::endl;

			std::vector<unsigned int>
				vs(valsums(_Table));

			unsigned int max_val =
				*std::max_element(vs.cbegin(), vs.cend());
			
			size_t fcount = _Table.fields_count();
			
			std::string X("XXX");

			size_t cells_cout = _Table.fields_count(),
					slash_len = cells_cout * (CELL_WIDE + 1) + 1;

			print_slash(slash_len);

			print_empty_rows(2, cells_cout);

			while (max_val != 0) {
				
				std::cout << '|';

				for (size_t i = 0; i < fcount; ++i) {

					if (vs[i] >= max_val) {
						print_in_center(X, CELL_WIDE);
					} else 
						print_in_center("", CELL_WIDE);

					std::cout << '|';

				}

				--max_val;

				std::cout << std::endl;
			}
			
			print_slash(slash_len);

		}

	void print_names_row(const table& t) {
		table::const_iterator
			itr(t.cbegin()),
			end(t.cend());
		std::cout << '|';
		while (itr != end) {
			print_in_center(
				(itr++)->first, CELL_WIDE);
			std::cout << '|';
		}
		std::cout << std::endl;
	}

	void vertical_diagram
		::update() {
			print_val_columns();
			print_names_row(_Table);
			size_t cells_cout = _Table.fields_count(),
				slash_len = cells_cout * (CELL_WIDE + 1) + 1;
			print_slash(slash_len);
		}
}