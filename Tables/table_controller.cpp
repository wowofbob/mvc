#include "stdafx.h"
#include "table_controller.h"

#include "errors.h"
#include "parse.h"
#include "stdin_input.h"
#include "table.h"
#include "view.h"

#include <assert.h>
#include <string>
#include <iostream>
#include <memory>
#include <utility>

namespace meta {

	//  !!!
	const size_t MAX_NAME_LEN = 10;
	//

	void show_commands() {
		std::cout
			<< "    Tying a view:" << std::endl
			<< "        tie <view_type>" << std::endl
			<< "    Untying a view:" << std::endl
			<< "        untie <view_type>" << std::endl
			<< "    where" << std::endl
			<< "        view_type:" << std::endl
			<< "            h  : horizontal" << std::endl
			<< "            v  : vertical" << std::endl
			<< "            hd : horizontal diagram" << std::endl
			<< "            vd : vertical diagram" << std::endl
			<< "    Value setting:" << std::endl
			<< "        set <name> <pos> <val>" << std::endl
			<< "    Expanding (to left):" << std::endl
			<< "        expand <name> [<init_val>]" << std::endl
			<< "    Constricting (from left):" << std::endl
			<< "        constrict" << std::endl
			<< "    Stretching (to down):" << std::endl
			<< "        stretch [<init_val>]" << std::endl
			<< "    Compressing:" << std::endl
			<< "        compress" << std::endl << std::endl
			<< "    Show views: show" << std::endl
			<< "    Exit: exit" << std::endl;
	}


	const std::string cmd_exit("exit");
	const std::string cmd_help("help");


	const std::string cmd_tie("tie");
	const std::string cmd_untie("untie");
	const std::string vt_h("h");
	const std::string vt_v("v");
	const std::string vt_hd("hd");
	const std::string vt_vd("vd");

	const std::string cmd_show("show");

	const std::string cmd_expand("expand");
	const std::string cmd_constrict("constrict");
	const std::string cmd_stretch("stretch");
	const std::string cmd_compress("compress");
	const std::string cmd_set("set");

	bool is_param_missed(const parser& scan) {
		bool param_missed = !scan.has_next();
		if (param_missed) {
			print_param_missing_err();
		}
		return param_missed;
	}

	view* view_by_type(
		const std::string& view_type,
		const table& t) {
		if (view_type == vt_h) {
			return new horizontal(t);
		}
		else
		if (view_type == vt_v) {
			return new vertical(t);
		}
		else
		if (view_type == vt_hd) {
			return new horizontal_diagram(t);
		}
		else
		if (view_type == vt_vd) {
			return new vertical_diagram(t);
		}
		print_view_type_err();
		return 0;
	}

	void table_controller::
		parse_tying(parser& scan) {
			if (!is_param_missed(scan)) {
				std::string view_type(scan.token());
				if (has_view(view_type)) {
					print_tying_err();
				}
				else {
					view* new_view = view_by_type(view_type, _Table);
					tie_view(view_type, new_view);
				}
			} else
				print_param_missing_err();
		}

	void table_controller::
		parse_untying(parser& scan) {
			if (!is_param_missed(scan)) {
				std::string view_type(scan.token());
				if (!has_view(view_type)) {
					print_untying_err();
				}
				else {
					view* to_erase = get_view(view_type);
					untie_view(view_type, to_erase);
					_Table.untie(to_erase);
				}
			} else
				print_param_missing_err();
		}

	bool is_correct_cplx_name(
		std::pair<std::string, bool> cplx_name) {
		if (!cplx_name.second) {
			print_parse_name_err();
			return false;
		}
		if (cplx_name.first.size() > MAX_NAME_LEN) {
			print_too_long_name_err();
			return false;
		}
		return true;
	}

	bool is_correct_number(
		const std::string& num_str) {
		if (!is_number(num_str)) {
			print_parse_number_err();
			return false;
		}
		return true;
	}

	void table_controller::
		do_expand(parser& scan) {

			if (is_param_missed(scan)) return;

			std::pair<std::string, bool>
				name(scan.complex());

			if (!is_correct_cplx_name(name)) return;

			if (_Table.has_name(name.first)) {
				print_has_field_err();
				return;
			}

			if (scan.has_next()) {
				std::string val_str(scan.token());
				if (is_correct_number(val_str)) {
					unsigned int init_val =
						std::stoul(val_str);
					_Table.expand(name.first, init_val);
				}
			}
			else {
				_Table.expand(name.first);
			}

		}

	bool is_has_fields(const table& t) {
		if (!t.fields_count()) {
			print_no_fields_err();
			return false;
		}
		return true;
	}

	bool is_has_values(const table& t) {
		if (!t.values_count()) {
			print_no_values_err();
			return false;
		}
		return true;
	}

	void table_controller::
		do_constrict() {
			if (is_has_fields(_Table)) {
				_Table.constrict();
			}
		}

	void table_controller::
		do_stretch(parser& scan) {

			if (!is_has_fields(_Table)) return;

			if (scan.has_next()) {
				std::string val_str(scan.token());
				if (is_correct_number(val_str)) {
					unsigned int init_val =
						std::stoul(val_str);
					_Table.stretch(init_val);
				}
			}
			else {
				_Table.stretch();
			}

		}

	void table_controller::
		do_compress() {
			if (is_has_values(_Table) && is_has_fields(_Table)) {
				_Table.compress();
			}
		}

	void table_controller::
		do_set(parser& scan) {

			if (is_param_missed(scan)) return;

			std::pair<std::string, bool>
				name(scan.complex());

			if (!is_correct_cplx_name(name)) return;

			if (!_Table.has_name(name.first)) {
				print_no_field_err();
				return;
			}

			if (is_param_missed(scan)) return;

			std::string pos_str(scan.token());
			if (!is_correct_number(pos_str)) return;

			size_t pos = std::stoul(pos_str);
			if (pos >= _Table.values_count()) {
				print_pos_out_of_range_err();
				return;
			}

			if (is_param_missed(scan)) return;

			std::string val_str(scan.token());
			if (is_correct_number(val_str)) {
				size_t val = std::stoul(val_str);
				_Table.get(name.first).set(pos, val);
			}
		}

	bool table_controller::
		parse_input() {

			std::cout << ":> ";
			std::string input(get_input());
			parser scan(input);
			if (!scan.has_next()) return true;

			// str -> const -> swtich (const) {}
			// str -> functor

			std::string
				command(scan.token());
			if (command == cmd_exit) {
				return false;
			}
			else
			if (command == cmd_help) {
				show_commands();
			}
			else
			if (command == cmd_show) {
				show();
			}
			else
			if (command == cmd_tie) {
				parse_tying(scan);
			}
			else
			if (command == cmd_untie) {
				parse_untying(scan);
			}
			else
			if (command == cmd_expand) {
				do_expand(scan);
			}
			else
			if (command == cmd_constrict) {
				do_constrict();
			}
			else
			if (command == cmd_stretch) {
				do_stretch(scan);
			}
			else
			if (command == cmd_compress) {
				do_compress();
			}
			else
			if (command == cmd_set) {
				do_set(scan);
			}
			else {
				print_unknown_cmd_err();
			}
			return true;
		}

	void table_controller
		::run() {

			while (parse_input()) {}

		}


	table_controller::
		table_controller(table& t) :
		controller(t), _Table(t) {
			run();
		}

}