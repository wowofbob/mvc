#ifndef META_TYPE_CHECK_H_
#define META_TYPE_CHECK_H_

#include <type_traits>

namespace meta {

	//Alises for enable_if::type
	template<bool Test, typename T = void>
	using enabled_if = typename std::enable_if<Test, T>::type;

	template<bool Test, typename T = void>
	using disabled_if = typename std::enable_if<!Test, T>::type;

	// `is_belong` structure checks whether the type
	// is an element of a type set (list of types)
	template<typename ThisType, typename... Types>
	struct is_belong;

	template<typename ThisType, typename ThatType, typename... Types>
	struct is_belong
		<ThisType,
		ThatType,
		Types...> : is_belong<ThisType, Types...>{};

	template<typename ThisType, typename... Types>
	struct is_belong
		<ThisType,
		ThisType,
		Types...> : std::true_type{};

	template<typename ThisType>
	struct is_belong<ThisType> : std::false_type{};

/*
	template<typename ThisType, typename... Types>
	constexpr bool is_belonged() { return is_belong<ThisType, Types...>::value; }
*/
	//`bare` is a pure type without any qualifications
	template<typename T>
	using removed_ref = typename std::remove_reference<T>::type;

	template<typename T>
	using removed_cv = typename std::remove_cv<T>::type;

	template<typename T>
	using bare = removed_cv<removed_ref<T> >;

	// `is_related` checks whether is the
	// first type related with others.
	
	template<typename U, typename... V>
	struct is_related : std::false_type {};

	template<typename U, typename V>
	struct is_related<U, V> : std::is_same<bare<U>, bare<V> >{};
	
	//
	//template<typename U, typename... V>
	//constexpr bool related_types() { return is_related<U, V...>::value; }

	//template<typename U, typename V>
	//constexpr bool same_types() { return std::is_same<U, V>::value; }

	////Checks whether is parameter pack empty

	//template<typename... Pack>
	//constexpr bool is_empty_pack() { return false; }

	//template<>
	//constexpr bool is_empty_pack() { return true; }

}

#endif//META_TYPE_CHECK_H_