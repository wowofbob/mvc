#ifndef META_MVC_ERRORS_H_
#define META_MVC_ERRORS_H_

namespace meta {

	void print_tying_err();
	void print_untying_err();
	void print_view_type_err();
	void print_unknown_cmd_err();

	void print_param_missing_err();

	void print_has_field_err();
	void print_no_field_err();

	void print_parse_number_err();

	void print_parse_name_err();
	void print_too_long_name_err();

	void print_pos_out_of_range_err();

	void print_no_fields_err();
	void print_no_values_err();

}

#endif//META_MVC_ERRORS_H_