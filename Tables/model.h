#ifndef META_MVC_MODEL_H_
#define META_MVC_MODEL_H_

#include <set>

#include <memory>

namespace meta {
	
	struct view;

	class model {
	protected:
		// Views which are subscribed on
		// the models updates notifications
		std::set<std::shared_ptr<view> > _Views;
		// raw -> shared_ptr

		model();

	public:
		// Subscribe `new_view` to notifications
		void tie(view* new_view);
		// Remove `view` from list of subsribed
		void untie(view* that_view);
		// Notifye all subscribed views
		void notify() const;
		//Allow inheritance
		virtual ~model();
	};

}

#endif//META_MVC_MODEL_H_