#ifndef META_MVC_TABLE_CONTROLLER_H_
#define META_MVC_TABLE_CONTROLLER_H_

#include "controller.h"
#include "parse.h"
#include "table.h"

namespace meta {

	class table_controller : public controller {

		table& _Table;

	public:

		void run();

		bool parse_input();
		void parse_tying(parser& scan);
		void parse_untying(parser& scan);
		
		void do_expand(parser& scan);
		void do_constrict();
		void do_stretch(parser& scan);
		void do_compress();
		void do_set(parser& scan);

		table_controller(table& t);

	};

}

#endif//META_MVC_TABLE_CONTROLLER_H_