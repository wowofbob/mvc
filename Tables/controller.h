#ifndef META_MVC_CONTROLLER_H_
#define META_MVC_CONTROLLER_H_

#include "view.h"
#include "model.h"

#include <map>
#include <string>

namespace meta {

	class controller {

	protected:

		model& _Model;

		std::map<std::string, view*> _Views;

	public:

		bool has_view(
			const std::string& view_name) const;

		view* get_view(
			const std::string& view_name) const;

		void tie_view(
			const std::string& view_name, view* inst);

		void untie_view(
			const std::string& view_name, view* inst);

		void show();

		controller(model& m);

		virtual ~controller();
	};

}

#endif//META_MVC_CONTROLLER_H_