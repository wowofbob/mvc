#ifndef META_MVC_STDIN_INPUT_H_
#define META_MVC_STDIN_INPUT_H_

#include "parse.h"
#include <iostream>
#include <string>

namespace meta {
		
	std::string get_input();

	class stdin_input {
		std::string _Str;
		str_citer _Fst;
		str_citer _Lst;
	public:
		inline const std::string& str() const;
		inline const str_citer& first() const;
		inline const str_citer& last() const;
		stdin_input();
	};

	const std::string&
		stdin_input::str() const { return _Str; }
	const str_citer&
		stdin_input::first() const { return _Fst; }
	const str_citer&
		stdin_input::last() const { return _Lst; }

}

#endif//META_MVC_STDIN_INPUT_H_