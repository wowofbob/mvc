#ifndef META_TABLE_H_
#define META_TABLE_H_

#include <list>
#include <map>
#include <string>
#include <vector>
#include <utility>

#include "field.h"
#include "model.h"

namespace meta {

	
/*
	class table : public model {
		
		std::map<std::string, size_t> _Name2Idx;

		std::vector<
			std::pair<
				std::string,
				std::vector<unsigned int> > > _Data;
	
	};
	*/
	
	
	
	
	class table : public model {
		
		std::map<std::string, field*> _Name2field;
		std::list<std::pair<std::string, field> > _Fields;

		size_t _Val_size;

	public:

		table();

		void stretch(unsigned int init_val = 0);
		void compress();

		void expand(std::string const& name, unsigned int init_val = 0);
		void expand(std::string&& name, unsigned int init_val = 0);
		void constrict();

		field const& get(std::string const& name) const;
		field& get(std::string const& name);

		typedef std::list<std::pair<std::string, field> >
			::const_iterator const_iterator;

		const_iterator cbegin() const;
		const_iterator cend() const;

		size_t fields_count() const;
		size_t values_count() const;

		bool has_name(std::string const& name) const;

	};


}

#endif//META_TABLE_H_