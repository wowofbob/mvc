#include "stdafx.h"
#include "field.h"

#include <assert.h>

#include <map>
#include <utility>

#include "model.h"

namespace meta {

	std::map<size_t, unsigned int>
		field::generate_n(size_t n, unsigned int init_val) {
			std::map<size_t, unsigned int> alloc;
			for (size_t i = 0; i < n; ++i) {
				alloc.insert(std::make_pair(i, init_val));
			}
			return alloc;
		}

	field::field(model const& parrent,
		size_t length, unsigned int init_val) :
		_Model(parrent), _Vals(generate_n(length, init_val)) {}

	field::const_iterator field::cbegin() const {
		return _Vals.cbegin();
	}

	field::const_iterator field::cend() const {
		return _Vals.cend();
	}

	unsigned int
		field::get(size_t pos) const {
			const_iterator target(_Vals.find(pos));
			assert(target != cend());
			return target->second;
		}

	void field::set(size_t pos, unsigned int val) {
		assert(pos < size());
		_Vals[pos] = val;
		_Model.notify();
	}

	size_t field::size() const { return _Vals.size(); }

	void field::stretch(unsigned int init_val) {
		_Vals.insert(std::make_pair(size(), init_val));
	}

	void field::compress() {
		assert(size() > 0);
		const_iterator last(_Vals.find(size() - 1));
		_Vals.erase(last);
	}

}