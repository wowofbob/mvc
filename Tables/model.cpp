#include "stdafx.h"
#include "model.h"

#include <algorithm>
#include <set>

#include "view.h"

namespace meta {

	model::model() : _Views() {}
	
	// Subscribe `new_view` to notifications
	void model::tie(view* new_view) {
		_Views.insert(std::shared_ptr<view>(new_view));
	}

	class is_eq_to {
		view* _View;
	public:
		
		is_eq_to(view* v) : _View(v) {}

		bool operator()(const std::shared_ptr<view>& sh_View) const {
			return sh_View.get() == _View;
		}
	};

	// Remove `view` from list of subsribed
	void model::untie(view* that_view) {
		std::set<std::shared_ptr<view> >
			::const_iterator
			itr(_Views.cbegin()),
			end(_Views.cend()),
			targ(std::find_if(itr, end, is_eq_to(that_view)));
		if (targ != end)
			_Views.erase(targ);
	}

	// Notifye all subscribed views
	void model::notify() const {
		// Iterator is non constant because
		// view could makes some changings
		// upon getting notification
		std::set<std::shared_ptr<view> >::iterator
			next(_Views.begin()), last(_Views.end());
		while (next != last) {
			(*(next++))->update();
		}
	}

	model::~model() {}

}